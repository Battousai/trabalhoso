#!/bin/bash

getLicenciaturas(){
    cat $UTILIZADORES | cut -d',' -f3 | sort | uniq
    return 0
}

getUsernames(){
    cat $UTILIZADORES | cut -d',' -f1
    return 0
}

utilizadorMaisLigacoes() {
    local filename
    local networkCount
    local maxUser
    local maxCount=0

    for user in $1; do
        filename=$(getUserNetworkFile $user)
        if test ! -f $filename ; then
            continue
        fi

        networkCount=$(cat $filename | wc -l)
        if test $networkCount -gt $maxCount ; then
            maxCount=$networkCount
            maxUser=$user
        fi
    done

    echo "$maxUser" "$maxCount"
    return 0
}

licenciaturaMaisLigacoes() {
    local licenciaturaMax=""
    local licenciaturaMaxLigacoes=0

    for licenciatura in $1
    do
        local ligacoesLicenciatura=0
        local usernames=$(cat $UTILIZADORES | grep ",$licenciatura," | cut -d',' -f1)
        for username in $usernames
        do
            local fileName=$(getUserNetworkFile $username)
            if [ -f $fileName ]; then
                ligacoesLicenciatura=$((ligacoesLicenciatura + $(cat $fileName | wc -l)))
            fi
        done

        if [ $ligacoesLicenciatura -gt $licenciaturaMaxLigacoes ]; then
            licenciaturaMaxLigacoes=$ligacoesLicenciatura
            licenciaturaMax=$licenciatura
        fi
    done

    echo "$licenciaturaMax" "$licenciaturaMaxLigacoes"
    return 0
}

mesDoAnoComMaisLigacoes() {
    local files=$(ls network_* 2>/dev/null)

    if test ! -n files ; then
        return 1
    fi

    local tmpFile=$(mktemp)

    for f in $files; do
        cat $f >> $tmpFile
    done

    cat $tmpFile | cut -d'-' -f1,2 | sort | uniq -c | sort -r | head -1
    return 0
}

estatisticas(){

    local licenciaturas=$(getLicenciaturas)

    echo " ** ESTATISTICAS ** "

    echo "• Contagem do número de alunos por licenciatura "
    for l in $licenciaturas
    do
        echo -n $l ":  "
        cat $UTILIZADORES | grep ",$l," | wc -l
    done

    echo "• Cinco principais dominios de emails dos utilizadores registados"
    cat $UTILIZADORES | cut -d',' -f1 | cut -d'@' -f2 | sort | uniq -c | sort -r | head -5

    echo "• Utilizador com mais ligações"
    utilizadorMaisLigacoes "$(getUsernames)"

    echo "• Licenciatura com mais ligações"
    licenciaturaMaisLigacoes "$licenciaturas"

    echo "• Mês do ano com mais ligações"
    mesDoAnoComMaisLigacoes

    return 0
}

# Sair se estamos a ser importados
[[ "$1" == "source" ]] && return 0

source common.sh

estatisticas
