#!/bin/bash

addConnection(){
    echo "`date +%Y-%m-%d` $1" >> $file
    return 0
}

adiciona(){

    read -p "User 1: " user1
    read -p "User 2: " user2

    local file=$(getUserNetworkFile $user1)

    if ! userExists $user1 ; then
        echo "utilizador $user1 não existe no ficheiro."
        return 1
    fi


    if ! userExists $user2 ; then
        echo "utilizador $user2 não existe no ficheiro."
        return 1
    fi

    if connectionExists $file $user2 ; then
        echo "ligação já existe para o utilizador $user2."
        return 1
    fi

    addConnection $user2
    echo "Ligação adicionada !"
    return 0
}

# Sair se estamos a ser importados
[[ "$1" == "source" ]] && return 0

source common.sh

adiciona
