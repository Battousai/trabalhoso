#!/bin/bash

randomString() {
    readonly local DEFAULT_SIZE=10
    local size=${1:-$DEFAULT_SIZE}

    # cat /dev/urandom - Lixo Aleatório
    # tr -dc 'a-zA-Z0-9' - Filtragem
    # fold -w $size - Insere newline após X caracteres
    # head -n 1 - primeira linha
    local randomString=$( \
        cat /dev/urandom \
        | tr -dc 'a-zA-Z0-9' \
        | fold -w $size \
        | head -n 1 \
    )
    echo "$randomString"
    return 0
}

hashString() {
    local hashedPwd=$( \
        echo -n $1 \
        | sha1sum \
        | cut -d' ' -f1 \
    )
    echo "$hashedPwd"
    return 0
}

addUtilizador() {
    echo "$1,$2,$3,$4" >> $UTILIZADORES
    return $?
}

alertUserNewAccount() {
    local body="Password actual: $2.
        Deve alterar a sua password."
    res=$(echo $body | mail -s "Conta criada" "$1")
    return $?
}

isValidEmailAddress() {
    r=$(echo $1 | egrep "^(([-a-zA-Z0-9\!#\$%\&\'*+/=?^_\`{\|}~])+\.)*[-a-zA-Z0-9\!#\$%\&\'*+/=?^_\`{\|}~]+@\w((-|\w)*\w)*\.(\w((-|\w)*\w)*\.)*\w{2,4}$")
    return $?
}

novo_util() {
    read -p "Username: " username

    if ! isValidEmailAddress $username ; then
        echo "Username tem que ser endereço de email válido!"
        return 1
    fi

    if userExists $username ; then
        echo "Username já existe"
        return 1
    fi

    read -p "Nome: " nome
    read -p "Curso: " curso
    curso=$(echo $curso | tr 'a-z' 'A-Z')

    local password=$(randomString)
    local hashedPassword=$(hashString $password)

    addUtilizador "$username" "$nome" "$curso" "$hashedPassword"
    alertUserNewAccount "$username" "$password"
}

# Sair se estamos a ser importados
[[ "$1" == "source" ]] && return 0

source common.sh

novo_util
