#!/bin/bash

readonly UTILIZADORES="utilizadores.txt"

userExists() {
    if [ ! -f $UTILIZADORES ]; then
        return 1
    fi
    r=$(cat $UTILIZADORES | grep "$1")
    return $?
}

connectionExists() {
    if [ ! -f $1 ]; then
        return 1
    fi
    r=$(cat $1 | grep "$2")
    return $?
}

sanitizeUsername() {
    echo $1 | tr '@' '.'
    return $?
}

getUserNetworkFile() {
    echo "network_$(sanitizeUsername $1)"
    return 0
}
