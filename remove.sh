#!/bin/bash

remove() {

    read -p "User 1: " user1
    if ! userExists $user1 ; then
        echo "$user1 não existe."
        return 1
    fi

    read -p "User 2: " user2
    if ! userExists $user2 ; then
        echo "$user2 não existe."
        return 1
    fi


    local file=$(getUserNetworkFile $user1)

    if test ! -f $file ; then
        echo "Ficheiro não existente !"
        return 1
    fi

    if ! connectionExists $file $user2 ; then
        echo "Ligação inexistente!"
        return 1
    fi

    # Retorna o conteudo do ficheiro sem a ligação a ser apagada
    local newText=`cat $file | grep -v $user2`

    # Actualiza ficheiro sem a ligaçao removida
    echo $newText > $file
    echo "utilizador $user2 foi removido da lista de $user1."
    return 0
}

# Sair se estamos a ser importados
[[ "$1" == "source" ]] && return 0

source common.sh

remove
