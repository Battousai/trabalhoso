#!/bin/bash

source common.sh
source novo_util.sh "source"
source adiciona.sh "source"
source remove.sh "source"
source estatisticas.sh "source"
source backup.sh "source"

while true; do

    cat <<EOF
------------------------
Administração SnapISCTE:
------------------------
1. Criação de utilizador
2. Adicionar ligação de utilizadores
3. Remoção de ligação
4. Backup
5. Estatísticas
6. Sair
EOF

    read -p "Escolha: " opt

    case "$opt" in
      1) novo_util
      ;;
      2) adiciona
      ;;
      3) remove
      ;;
      4) backup
      ;;
      5) estatisticas
      ;;
      6) break
      ;;
      *) echo "Opção não implementada"
    esac

done

exit 0
