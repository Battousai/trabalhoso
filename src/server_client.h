#define MESSAGE_KEY 555444

#define ERR_MSG_CREATION    700
#define ERR_MSG_RECEIVING   701
#define ERR_MSG_SEND        702

#define RESULT_AUTH_SUCCESS             0
#define RESULT_AUTH_USERNAME_NOT_FOUND  1
#define RESULT_AUTH_INVALID_PASSWORD    3

typedef struct {
    long tipo;
    int PID;
    char username[40];
    char pass_sha1[42];
    int resultado;
    char access_token[21];
    char mensagem[182];
} SnapStruct;

#define MSG_SIZE sizeof(SnapStruct) - sizeof(long)
#define MSG_TYPE_AUTH 1
#define MSG_TYPE_TEXT 2

int msgGet();
void msgAuthRcv(int id, SnapStruct* ss);
void msgTextRcv(int id, SnapStruct* ss);
void msgRcv(int id, SnapStruct* ss, int msgType);
void msgAuthSend(int id, char* username, char* pass_sha1, int pid);
void msgSendNoUserWithSuchUsername(int id, int pid);
void msgSendInvalidPassword(int id, int pid);
void msgSendAuthSuccess(int id, int pid, char* access_token);
void msgSend(int id, SnapStruct* snap);
void msgSendTextNetwork(int id, int pid, char* message);
void msgSendText(int id, char* username, char* access_token, char* message);
