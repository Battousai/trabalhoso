#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "users.h"
#include "file_utils.h"
#include "semaphore_helpers.h"
#include "shm_helpers.h"
#include "server_client.h"

void carregarUsers();
void reloadUsers(int signal);
void closeProcess();
void cleanUp();
void limparUsers();
void listenAuthMessages();
void listenTextMessages();
Tuser* getUserByUsername(char* username);
Tuser* getUserByUsernameAndToken(char* username, char* token);
void rand_str(char *dest, size_t length);
void initUserSession(Tuser* user, int pid);
void authenticateUser(int msgId, SnapStruct* snap);
void printSummary();
void sendMessageToNetwork(int msgId, SnapStruct* snap);

Tuser *users;
size_t nUsers;
int shmId;
int semId;

int main()
{
    if (isStatsAlreadyRunning()) {
        perror("Statsd já está a correr. Saindo...");
        return EXIT_FAILURE;
    }

    if (fork() == 0) { // filho snapstatsd
        if (execl("./snapstatsd", "snapstatsd", NULL) == -1) {
            puts("Não consegui lançar o snapstatsd... saindo");
            return EXIT_FAILURE;
        }
    }

    semId = semGet();

    printf("PID: %d\n", getpid());
    carregarUsers();

    if (fork() == 0) { // filho mensagens texto
        listenTextMessages();
        return EXIT_SUCCESS;
    }

    signal(SIGUSR1, reloadUsers);
    signal(SIGINT, closeProcess);

    listenAuthMessages();

    return EXIT_SUCCESS;
}

void listenTextMessages()
{
    int msgId = msgGet();
    SnapStruct snap;

    while (1) {
        puts("listening text messages...");
        msgTextRcv(msgId, &snap);
        sendMessageToNetwork(msgId, &snap);
    }
}

void sendMessageToNetwork(int msgId, SnapStruct* snap)
{
    size_t i, j;
    Tuser* user = getUserByUsernameAndToken(snap->username, snap->access_token);

    if (user == NULL) {
        return;
    }

    for (i = 0; i < user->nlinks; i++)
        for (j = 0; j < nUsers; j++)
            if (users[j].last_pid > 0 &&
                strcmp(users[j].username, user->ligacoes[i].user) == 0)
            {
                msgSendTextNetwork(msgId, users[j].last_pid, snap->mensagem);
                printf("Message sent from \"%s\" to \"%s\"\n", snap->username,
                    users[j].username);
                break;
            }
}

void listenAuthMessages()
{
    int msgId = msgGet();
    SnapStruct snap;

    while (1) {
        puts("listening auth messages...");
        msgAuthRcv(msgId, &snap);
        authenticateUser(msgId, &snap);
    }
}

// autenticação de utilizador
void authenticateUser(int msgId, SnapStruct* snap)
{
    Tuser* user = getUserByUsername(snap->username);
    if (user == NULL) {
        puts("NoUserWithSuchUsername");
        msgSendNoUserWithSuchUsername(msgId, snap->PID);
        printf("Auth error: No user with such username \"%s\"\n", snap->username);
        return;
    }

    if (strcmp(user->pass_sha1, snap->pass_sha1) != 0) {
        puts("InvalidPassword");
        msgSendInvalidPassword(msgId, snap->PID);
        printf("Auth error: Invalid password for user \"%s\"\n", snap->username);
        return;
    }

    printf("Auth Success for user \"%s\"\n", snap->username);
    initUserSession(user, snap->PID);
    msgSendAuthSuccess(msgId, snap->PID, user->access_token);
}

// inicializa sessão de utilizador, definindo pid e token
void initUserSession(Tuser* user, int pid)
{
    semDown(semId);
    user->last_pid = pid;
    rand_str(user->access_token, ACCESS_TOKEN_SIZE - 1);
    semUp(semId);
}

// carrega utilizadores para shm memory
void carregarUsers()
{
    size_t i;

    nUsers = countLines(USERS_FILE);
    shmId = shmCreate(sizeof(Tuser) * nUsers);
    semDown(semId);
    users = shmAt(shmId);
    fetchUsers(users);
    for (i = 0; i < nUsers; i++) {
        char* nn = networkName(users[i].username);
        fetchLinks(nn, &users[i]);
        free(nn);
    }
    semUp(semId);
}

// limpa utilizadores, carrega para memória, imprime
void reloadUsers(int sinal)
{
    limparUsers();
    carregarUsers();
    printSummary();
}

void printSummary()
{
    size_t i, nTotalLigs = 0, nCursos = 0;
    char cursos[CURSO_SIZE * nUsers + nUsers];
    // limpar memória de cursos
    memset(&cursos[0], 0, sizeof(cursos));

    //obter numero total de ligacoes e cursos diferentes
    for (i = 0; i < nUsers; i++) {

        nTotalLigs += users[i].nlinks;

        if (strstr(cursos, users[i].curso) == NULL) {
            strcat(cursos, users[i].curso);
            strcat(cursos, "_");
            nCursos++;
        }
    }

    printf("Utilizadores e ligações carregadas com sucesso!\n");
    printf("  Nº de utilizadores: %zu\n", nUsers);
    printf("  Nº de cursos diferentes: %zu\n", nCursos);
    printf("  Nº total de ligações: %zu\n\n", nTotalLigs);
}

// encontra utilizadores pelo seu username
Tuser* getUserByUsername(char* username)
{
    size_t i;
    for (i = 0; i < nUsers; i++) {
        if (strcmp(username, users[i].username) == 0) {
            return &users[i];
        }
    }

    return NULL;
}

// encontra utilizadores pelo seu username e token
Tuser* getUserByUsernameAndToken(char* username, char* token)
{
    size_t i;
    for (i = 0; i < nUsers; i++) {
        if (strcmp(username, users[i].username) == 0 && strcmp(token, users[i].access_token) == 0) {
            return &users[i];
        }
    }

    return NULL;
}

// limpa shm de utilizadores
void limparUsers()
{
    semDown(semId);
    shmRem(shmId, users);
    semUp(semId);
}

void cleanUp()
{
    limparUsers();
    semRm(semId);
}

void closeProcess(int signal)
{
    cleanUp();
    if (isStatsAlreadyRunning()) {
        kill(readStatsPid(), SIGTERM);
    }
    printf("Processo Parado : snapiscte \n");
    exit(EXIT_SUCCESS);
}

// http://stackoverflow.com/questions/15767691/whats-the-c-library-function-to-generate-random-string
void rand_str(char *dest, size_t length)
{
    char charset[] = "0123456789"
    "abcdefghijklmnopqrstuvwxyz"
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    while (length-- > 0) {
        size_t index = (double) rand() / RAND_MAX * (sizeof charset - 1);
        *dest++ = charset[index];
    }
    *dest = '\0';
}
