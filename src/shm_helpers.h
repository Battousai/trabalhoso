#define SHM_USERS_KEY 1337

#define SHM_ERR_CANT_ALOCATE 500
#define SHM_ERR_CANT_INITIALIZE 501
#define SHM_ERR_DETACH 502
#define SHM_ERR_REMOVE 503

int shmGet(size_t size);
int shmCreate(size_t size);
Tuser* shmAt(int shmId);
void shmRem(int shmId, Tuser*);
