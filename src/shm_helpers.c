#include <stdlib.h>
#include <stdio.h>
#include <sys/shm.h>
#include "users.h"
#include "shm_helpers.h"

int shmCreate(size_t size)
{
    int shmId;
    shmId = shmget(SHM_USERS_KEY, size, IPC_CREAT | IPC_EXCL | 0620);

    if (shmId < 0) {
        perror("Erro na criação de memória partilhada. Saindo...");
        exit(SHM_ERR_CANT_ALOCATE);
    }

    return shmId;
}

int shmGet(size_t size)
{
    int shmId;
    shmId = shmget(SHM_USERS_KEY, size, 0620);

    if (shmId < 0) {
        perror("Erro na leitura de memória partilhada. Saindo...");
        exit(SHM_ERR_CANT_ALOCATE);
    }

    return shmId;
}

Tuser* shmAt(int shmId)
{
    Tuser* users;
    users = shmat(shmId, 0, 0);
    if (users == NULL) {
        perror("Erro no shmat");
        exit(SHM_ERR_CANT_INITIALIZE);
    }
    return users;
}

void shmRem(int shmId, Tuser* users)
{
    if (shmdt(users) < 0) {
        perror("Error on detaching memory");
        exit(SHM_ERR_DETACH);
    }

    if (shmctl(shmId, IPC_RMID, NULL) < 0) {
        perror("Error on removing shared memory");
        exit(SHM_ERR_REMOVE);
    }
}
