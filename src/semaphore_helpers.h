#define SEM_KEY 1338

int semGet();
void semDown(int semID);
void semUp(int semId);
void semRm(int semId);
