#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include "server_client.h"

void sha1sum(char pass[], char hash[]) {
    // cria 2 pipes
    int pin[2], pout[2];
    pipe(pin);
    pipe(pout);
    // cria um processo filho para executar o comando "sha1sum"
    int n = fork();
    if ( n == 0 ) {
        close(pin[0]);
        close(pout[1]);
        dup2(pin[1], 1 );
        dup2(pout[0], 0);
        execl("/usr/bin/sha1sum", "sha1sum", NULL );
        exit(1);
    }
    close(pin[1]);
    close(pout[0]);
    // escreve a password no pipe "pout", que vai ser o input do sha1sum
    FILE *f;
    f = fdopen( pout[1], "w" );
    fprintf(f, "%s", pass );
    fclose(f);
    // le o resultado atraves do pipe "pin", que e o output do sha1sum
    f = fdopen(pin[0], "r" );
    fgets(hash, 100, f);
    fclose(f);
    wait(NULL);
}


int main()
{

    SnapStruct ss;
    int msgid, op,i;
    char username[sizeof(ss.username)];
    char password[12];
    char pass_sha1[sizeof(ss.pass_sha1)];
    msgid=msgGet();

    printf("\n ** SNAPCLIENT **\n Login\n  Username: ");
    fgets(username, sizeof(username), stdin);
    username[strlen(username)-1] = '\0';

    printf("  Password: ");
    fgets(password, sizeof(password), stdin);
    password[strlen(password)-1] ='\0';
    sha1sum(password, pass_sha1);
    for(i=0; i< strlen(pass_sha1); i++)
    {
        if(pass_sha1[i] == ' ')
        {
            pass_sha1[i] = '\0';
            break;
        }
    }
    //pass_sha1[strlen(pass_sha1)-4]='\0';

    //printf("%s   %s    %s\n", username, password, pass_sha1);

    msgAuthSend(msgid, username, pass_sha1, getpid());
    printf("\n A autenticar...\n");

    msgRcv(msgid, &ss, getpid());
    //printf("Resultado: %d\n", ss.resultado);

    if(ss.resultado == 1)
    {
        printf(" Erro na autenticação: utilizador inexistente... \n A fechar aplicação...\n ");
        exit(EXIT_FAILURE);
    }
    else if(ss.resultado == 3)
    {

        printf(" Erro na autenticação: password incorrecta... \n A fechar aplicação...\n ");
        exit(EXIT_FAILURE);
    }
    else if(ss.resultado == 0)
    {
        op=0;
        while(op != 3)
        {
            printf("\n ** MENU **\n Escolha uma opção:\n  1. Ler mensagens\n  2. Escrever mensagens\n  3. Sair\n");
            scanf("%d%*c", &op);
            if(op == 1)
            {
                printf("\n A ler mensagens...\n");
                while(1)
                {
                    msgRcv(msgid, &ss, getpid());
                    printf("%s\n", ss.mensagem);
                }
            }
            else if(op == 2)
            {
                char msg[140],msgF[sizeof(ss.mensagem)];
                printf("\n Escreva uma mensagem: \n");
                fgets(msg, sizeof(msg), stdin);
                msg[strlen(msg)-1]='\0';

                strcpy(msgF,username);
                strcat(msgF, ":");
                strcat(msgF, msg);


                msgSendText(msgid, username, ss.access_token, msgF);

            }

        }

    }

    exit(EXIT_SUCCESS);

}
