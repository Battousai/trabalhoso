#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "file_utils.h"

size_t countLines(const char* filename)
{
    FILE* f = fileRead(filename);

    if (f == NULL) {
        return 0;
    }

    int lines = 0;

    while ( ! feof(f)) {
        if (fgetc(f) == '\n') {
            lines++;
        }
    }

    fclose(f);
    return lines;
}

char* networkName(const char* username)
{
    char* normalUsername = strdup(username);
    char* replace = strstr(normalUsername, "@");
    strncpy(replace, ".", 1);

    char* networkName = malloc(sizeof(char) * 256);
    snprintf(networkName, sizeof(char) * 256,
        "%s%s", NETWORK_FILE_PREFIX, normalUsername);
    free(normalUsername);

    return networkName;
}

int isStatsAlreadyRunning()
{
    int pid = readStatsPid();

    if (pid == -1 || isProcessRunning(pid) == -1) {
        return 0;
    }

    return 1;
}

int isProcessRunning(int pid)
{
    if (pid <= 0) return -1;

    return kill(pid, 0);
}

int readStatsPid()
{
    int pid;
    FILE* f = fileRead(PID_FILE);

    if (f == NULL) {
        return -1;
    }

    fscanf(f, "%d", &pid);

    fclose(f);
    return pid;
}

FILE* fileRead(const char* filename)
{
    if (access(filename, R_OK) == -1) {
        return NULL;
    }

    return fopen(filename, "r");
}
