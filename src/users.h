#define USERNAME_SIZE 40
#define CURSO_SIZE 20
#define ACCESS_TOKEN_SIZE 21

typedef struct {
    char user[USERNAME_SIZE];
    char data[12];
} Tlinks;

typedef struct {
    char username[USERNAME_SIZE];
    char ncompleto[60];
    char curso[CURSO_SIZE];
    char pass_sha1[42];
    Tlinks ligacoes[100];
    size_t nlinks;
    char access_token[ACCESS_TOKEN_SIZE];
    int last_pid;
} Tuser;

void printUserData(Tuser* user, char* name, int userCount);
void printUsersData(Tuser* user, int userCount);
void fetchLinks(const char* network, Tuser* user);
void fetchUsers(Tuser* users);
