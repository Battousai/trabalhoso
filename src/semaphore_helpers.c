#include <stdlib.h>
#include <stdio.h>
#include <sys/sem.h>
#include "semaphore_helpers.h"

#define SEM_ERR_CANT_CREATE 600
#define SEM_ERR_CANT_INITIALIZE 601
#define SEM_ERR_CANT_DOWN 602
#define SEM_ERR_CANT_UP 603
#define SEM_ERR_REMOVE 604

struct sembuf SEM_DOWN = {0, -1, 0};
struct sembuf SEM_UP = {0, 1, 0};

int semGet()
{
    int semId = semget(SEM_KEY, 1, 0600 | IPC_CREAT);
    int status;

    if (semId < 0) {
        perror("Erro na criação de semáforo! Saindo...");
        exit(SEM_ERR_CANT_CREATE);
    }

    status = semctl(semId, 0, SETVAL, 1);
    if (status < 0) {
        perror("Erro na inicialização de semáforo! Saindo...");
        exit(SEM_ERR_CANT_INITIALIZE);
    }

    return semId;
}

void semDown(int semId)
{
    if (semop(semId, &SEM_DOWN, 1) < 0) {
        perror("Erro SEM_DOWN! Saindo...");
        exit(SEM_ERR_CANT_DOWN);
    }
}

void semUp(int semId)
{
    if (semop(semId, &SEM_UP, 1) < 0) {
        perror("Erro SEM_UP! Saindo...");
        exit(SEM_ERR_CANT_UP);
    }
}

void semRm(int semId)
{
    if (semctl(semId, 0, IPC_RMID) < 0) {
        perror("Erro ao remover semáforo");
        exit(SEM_ERR_REMOVE);
    }
}
