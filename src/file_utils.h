#define NETWORK_FILE_PREFIX "../network_"
#define USERS_FILE "../utilizadores.txt"
#define PID_FILE "snapstatsd.pid"

size_t countLines(const char* filename);
char* normalizedUsername(const char* username);
char* networkName(const char* username);
int isStatsAlreadyRunning();
int readStatsPid();
FILE* fileRead(const char* filename);
int isProcessRunning(int pid);
