#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "users.h"
#include "file_utils.h"

void printUsersData(Tuser* user, int userCount)
{
    size_t i = 0;
    for (; i < userCount; i++) {

        printf("\n\n----------------------------\n");
        printf("Informação de utilizador\n");
        printf("----------------------------\n");

        printf("Username: %s \nCurso: %s \n", user[i].username, user[i].curso);
        printf("Nome Completo: %s \n", user[i].ncompleto);

        printf("Ligações User:\n");

        size_t j = 0;
        for (; j < user[i].nlinks; j++) {
            // puts(user[i].ligacoes[j].user);
            printf("user : %s  data : %s \n",
            user[i].ligacoes[j].user,
            user[i].ligacoes[j].data);
        }

        printf("----------------------------\n");
    }
}

void printUserData(Tuser* user, char* name, int userCount)
{
    size_t i = 0;
    for (; i < userCount; i++) {

        if (strncmp(user[i].username, name, USERNAME_SIZE) != 0) {
            continue;
        }

        printf("\n\n----------------------------\n");
        printf("Informação de utilizador\n");
        printf("----------------------------\n");

        printf("Username: %s \nCurso: %s \n", user[i].username, user[i].curso);
        printf("Nome Completo: %s \n", user[i].ncompleto);

        printf("Ligações User:\n");

        size_t j = 0;
        for (; j < user[i].nlinks; j++) {
            // puts(user[i].ligacoes[j].user);
            printf("user : %s  data : %s \n",
            user[i].ligacoes[j].user,
            user[i].ligacoes[j].data);
        }

        printf("----------------------------\n");

        return;

    }

    printf("\nO utilizador não existe!\n\n");

}

void fetchLinks(const char* network, Tuser* user)
{
    FILE* f = fileRead(network);

    if (f == NULL) {
        return;
    }

    char line[1024];
    size_t i = 0;
    for (; fgets(line, 1024, f); i++) {
        strncpy(user->ligacoes[i].data, strtok(line, " "), 12);
        strncpy(user->ligacoes[i].user, strtok(NULL, "\n"), USERNAME_SIZE);
    }
    user->nlinks = i;

    fclose(f);
}

void fetchUsers(Tuser* users)
{
    FILE* f = fileRead(USERS_FILE);

    if (f == NULL) {
        perror("Can't read USERS_FILE");
        exit(EXIT_FAILURE);
    }

    char line[1024];
    size_t i = 0;
    for (; fgets(line, 1024, f); i++) {
        strncpy(users[i].username, strtok(line, ","), USERNAME_SIZE);
        strncpy(users[i].ncompleto, strtok(NULL, ","), 60);
        strncpy(users[i].curso, strtok(NULL, ","), 20);
        strncpy(users[i].pass_sha1, strtok(NULL, "\n"), 42);
        users[i].last_pid = 0;
    }

    fclose(f);
}
