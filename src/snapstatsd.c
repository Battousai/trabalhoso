#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <time.h>
#include "file_utils.h"
#include "users.h"
#include "file_utils.h"
#include "semaphore_helpers.h"
#include "shm_helpers.h"


#define STATS_FILE "stats.txt"
#define DELAY_IN_SECONDS 300

void writePid()
{
    FILE* f = fopen(PID_FILE, "w");
    if (f == NULL) {
        printf("Erro ao abrir ficheiro!\n");
        exit(EXIT_FAILURE);
    }

    fprintf(f, "%d\n", getpid());
    fclose(f);
}

char* currentTime()
{
    const size_t timeSize = sizeof(char) * 20;
    char* currentTime = malloc(timeSize);
    struct tm st;
    time_t t;

    t = time(NULL);
    st = *gmtime(&t);

    snprintf(currentTime, timeSize,
        "%d-%02d-%02d %02d:%02d:%02d",
        1900 + st.tm_year, st.tm_mon + 1, st.tm_mday,
        st.tm_hour, st.tm_min, st.tm_sec);

    return currentTime;
}

void writeStats()
{
    size_t lineSize = 1024;
    FILE* usersRepo;
    FILE* stats;
    char* theTime = currentTime();

    if (access(USERS_FILE, R_OK) == -1) {
        return;
    }

    usersRepo = fopen(USERS_FILE, "r");
    stats = fopen(STATS_FILE, "w");

    if (usersRepo == NULL || stats == NULL) {
        return;
    }

    fprintf(stats, "%s\n", theTime);
    fprintf(stats, "Total utilizadores: %zd\n", countLines(USERS_FILE));
    char line[lineSize];
    while (fgets(line, lineSize, usersRepo)) {
        char* username = strtok(line, ",");
        char* network = networkName(username);

        fprintf(stats, "%s: %zd ligacoes\n", username, countLines(network));
        free(network);
    }

    fclose(usersRepo);
    fclose(stats);
    free(theTime);
}

void printSummary()
{
    size_t i, nTotalLigs = 0, nCursos = 0;
    int shmId,semId;
    Tuser *users;
    size_t nUsers = countLines(USERS_FILE);

    char cursos[CURSO_SIZE * nUsers + nUsers];
    // limpar memória de cursos
    memset(&cursos[0], 0, sizeof(cursos));

    shmId = shmGet(nUsers * sizeof(Tuser));
    users = shmAt(shmId);
    semId = semGet();
    semDown(semId);
    //obter numero total de ligacoes e cursos diferentes
    for (i = 0; i < nUsers; i++) {

        nTotalLigs += users[i].nlinks;

        if (strstr(cursos, users[i].curso) == NULL) {
            strcat(cursos, users[i].curso);
            strcat(cursos, "_");
            nCursos++;
        }
    }
    semUp(semId);
    printf("Utilizadores e ligações carregadas com sucesso!\n");
    printf("  Nº de utilizadores: %zu\n", nUsers);
    printf("  Nº de cursos diferentes: %zu\n", nCursos);
    printf("  Nº total de ligações: %zu\n\n", nTotalLigs);
}

void signalHandler(int signal)
{
    switch (signal) {
        case SIGALRM:
            //writeStats();
            printSummary();
            alarm(DELAY_IN_SECONDS);
        break;
        case SIGTERM:
            writeStats();
            printf("Bye bye\n");
            exit(EXIT_SUCCESS);
        break;
    }
}

int main()
{
    if (isStatsAlreadyRunning()) {
        puts("An instance is already running!");
        return EXIT_FAILURE;
    }

    writePid();

    //printf("PID: %d\n", getpid());

    signal(SIGALRM, signalHandler);
    signal(SIGTERM, signalHandler);

    alarm(DELAY_IN_SECONDS);

    while (1) {
        pause();
    }

    return EXIT_SUCCESS;
}
