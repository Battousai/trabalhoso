#include <stdlib.h>
#include <stdio.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>
#include "server_client.h"

int msgGet()
{
    int id = msgget(MESSAGE_KEY, IPC_CREAT | 0666);
    if (id < 0) {
        perror("Erro na criação / associação de mensagens");
        exit(ERR_MSG_CREATION);
    }

    return id;
}

// receive auth message
void msgAuthRcv(int id, SnapStruct* ss)
{
    msgRcv(id, ss, MSG_TYPE_AUTH);
}

// receive text message
void msgTextRcv(int id, SnapStruct* ss)
{
    msgRcv(id, ss, MSG_TYPE_TEXT);
}

// receive message
void msgRcv(int id, SnapStruct* ss, int msgType)
{
    while (msgrcv(id, ss, MSG_SIZE, msgType, 0) == -1) {
        if (errno == EINTR) continue; // recebemos um sinal, continuar
        perror("Erro no recebimento de mensagem");
        exit(ERR_MSG_RECEIVING);
    }
}

void msgAuthSend(int id, char* username, char* pass_sha1, int pid)
{
    SnapStruct ss;
    strncpy(ss.username, username, sizeof(ss.username));
    strncpy(ss.pass_sha1, pass_sha1, sizeof(ss.pass_sha1));
    ss.PID = pid;
    ss.tipo = MSG_TYPE_AUTH;
    msgSend(id, &ss);
}

void msgSendNoUserWithSuchUsername(int id, int pid)
{
    SnapStruct ss;
    ss.tipo = pid;
    ss.resultado = RESULT_AUTH_USERNAME_NOT_FOUND;
    msgSend(id, &ss);
}

void msgSendInvalidPassword(int id, int pid)
{
    SnapStruct ss;
    ss.tipo = pid;
    ss.resultado = RESULT_AUTH_INVALID_PASSWORD;
    msgSend(id, &ss);
}

void msgSendAuthSuccess(int id, int pid, char* access_token)
{
    SnapStruct ss;
    ss.tipo = pid;
    ss.resultado = RESULT_AUTH_SUCCESS;
    strncpy(ss.access_token, access_token, sizeof(ss.access_token));
    msgSend(id, &ss);
}

void msgSendTextNetwork(int id, int pid, char* message)
{
    SnapStruct ss;
    ss.tipo = pid;
    strncpy(ss.mensagem, message, sizeof(ss.mensagem));
    msgSend(id, &ss);
}

void msgSendText(int id, char* username, char* access_token, char* message)
{
    SnapStruct ss;
    ss.tipo = MSG_TYPE_TEXT;
    strncpy(ss.access_token, access_token, sizeof(ss.access_token));
    strncpy(ss.username, username, sizeof(ss.username));
    strncpy(ss.mensagem, message, sizeof(ss.mensagem));
    msgSend(id, &ss);
}

void msgSend(int id, SnapStruct* snap)
{
    if (msgsnd(id, snap, MSG_SIZE, 0) < 0) {
        perror("Erro no envio de mensagem!");
        exit(ERR_MSG_SEND);
    }
}
