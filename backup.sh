#!/bin/bash

backup() {
    local backupFile="backup_SnapISCTE_`date +%Y-%m-%d`.zip"
    if [ ! -f $backupFile ]; then
        zip -q $backupFile $UTILIZADORES network_*
        echo "Done"
        return 0
    fi

    echo "Ficheiro já existe!!!" >&2
    return 1
}

# Sair se estamos a ser importados
[[ "$1" == "source" ]] && return 0

source common.sh

backup
